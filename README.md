# iOSLightArray README #

iOSLightArray is the iOS implementation of LightArray. It's easy to use and a great way to work on LightArray without the complexities of embedded systems.

iOSLightArray references the LightArray repository as submodule so the two projects can remain independent. To start working with this repository use the git command

`git clone https://spencercarroll@bitbucket.org/spencercarroll/ioslightarray.git --recursive`

You can email me at: s c a r r o l l 1 5 7 8 @ g m a i l . c o m