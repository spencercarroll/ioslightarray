//
//  iOSPlatform.h
//  LightArrayiOS
//
//  Created by Spencer Carroll on 8/19/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#ifndef __LightArrayiOS__iOSPlatform__
#define __LightArrayiOS__iOSPlatform__

#ifdef __cplusplus
#include "light-array/Core/Platform.h"
#endif

/*
    This class acts primarily as a c++/objective c bridge.
 */

@protocol iOSPlatformPerformer <NSObject>
-(void)setChannel:(int)channel value:(CGFloat)value;
-(CGFloat)getChannel:(int)channel;
-(void)clearChannel:(int)channel;
-(void)clearAll;
-(void)update;
-(void)pause:(unsigned long) millis;
-(int)numChannels;
@end

#ifdef __cplusplus

class iOSPlatform : public Platform{
    
private:
    const id<iOSPlatformPerformer> performer;
    
public:
    iOSPlatform(const id<iOSPlatformPerformer> performer);
    virtual void setChannel(int channel, LAFloat value);
    virtual LAFloat getChannel(int channel);
    virtual void clearChannel(int channel);
    virtual void clearAll();
    virtual void update();
    virtual void pause(unsigned long millis);
    virtual const int numChannels();
    
};

#endif

#endif /* defined(__LightArrayiOS__iOSPlatform__) */
