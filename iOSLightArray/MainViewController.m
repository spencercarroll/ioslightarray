//
//  MainViewController.m
//  LightArrayiOS
//
//  Created by Spencer Carroll on 8/19/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#import "MainViewController.h"


@interface MainViewController ()
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *lights;

@end

@implementation MainViewController
@synthesize lights;
- (void)viewDidLoad
{
    [super viewDidLoad];
    for (UIView *lightView in lights) {
        [lightView.layer setCornerRadius:(lightView.frame.size.width/2.0)];
        lightView.contentMode = UIViewContentModeRedraw;
    }
}

-(void)setChannel:(int)channel value:(CGFloat)value{
    UIView *curLight = [lights objectAtIndex:channel];
    [curLight.layer setOpacity:value];
}
-(CGFloat)getChannel:(int)channel{
    UIView *curLight = [lights objectAtIndex:channel];
    return [curLight.layer opacity];
}
-(void)clearChannel:(int)channel{
    UIView *curLight = [lights objectAtIndex:channel];
    [curLight.layer setOpacity:0.0f];
}
-(void)clearAll{
    for (UIView *curLight in lights) {
        [curLight.layer setOpacity:0.0f];
    }
}
-(void)update{
    [CATransaction flush];
}
-(void)pause:(unsigned long) millis{
    NSTimeInterval timeInterval = millis/1000.0;//Convert millis to seconds.
    [NSThread sleepForTimeInterval:timeInterval];
}
-(int)numChannels{
    return lights.count;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
