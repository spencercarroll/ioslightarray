//
//  MainViewController.h
//  LightArrayiOS
//
//  Created by Spencer Carroll on 8/19/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iOSPlatform.h"
@interface MainViewController : UIViewController <iOSPlatformPerformer>

@end
