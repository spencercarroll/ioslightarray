//
//  iOSPlatform.cpp
//  LightArrayiOS
//
//  Created by Spencer Carroll on 8/19/14.
//  Copyright (c) 2014 Spencer Carroll. All rights reserved.
//

#include "iOSPlatform.h"



iOSPlatform::iOSPlatform(const id<iOSPlatformPerformer> performer): performer(performer){};

void iOSPlatform::setChannel(int channel, LAFloat value){
    [performer setChannel:channel value:value];
}
LAFloat iOSPlatform::getChannel(int channel){
    return [performer getChannel:channel];
}

void iOSPlatform::clearChannel(int channel){
    [performer clearChannel:channel];
}
void iOSPlatform::clearAll(){
    [performer clearAll];
}
void iOSPlatform::update(){
    [performer update];
}
void iOSPlatform::pause(unsigned long millis){
    [performer pause:millis];
}
const int iOSPlatform::numChannels(){
    return [performer numChannels];
}